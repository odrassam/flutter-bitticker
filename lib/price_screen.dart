import 'package:flutter/material.dart';
import 'coin_data.dart';
import 'package:flutter/cupertino.dart';
import 'dart:io' show Platform;
import 'coin_data.dart';

class PriceScreen extends StatefulWidget {
  @override
  _PriceScreenState createState() => _PriceScreenState();
}

class _PriceScreenState extends State<PriceScreen> {
  String selectedCurrency = 'USD';
  List<String> currencyValue = [];
  CoinData coinData = CoinData();

  @override
  void initState() {
    super.initState();
    getEachData('USD');
  }

  void getEachData(String currency) async {
    List bodyData = [];
    List<String> lastValue = [];
    for (String value in cryptoList) {
      var data = await coinData.showValue(value, currency);
      bodyData.add(data);
    }
    for (var value in bodyData) {
      String data = value['last'].toInt().toString();
      lastValue.add(data);
    }
    setState(() {
      selectedCurrency = currency;
      currencyValue = lastValue;
    });
  }

  DropdownButton<String> androidPicker() {
    List<DropdownMenuItem<String>> dropdownItems = [];
    for (String currency in currenciesList) {
      var newItem = DropdownMenuItem(
        child: Text(currency),
        value: currency,
      );
      dropdownItems.add(newItem);
    }

    return DropdownButton<String>(
      value: selectedCurrency,
      items: dropdownItems,
      onChanged: (value) {
        getEachData(value);
      },
    );
  }

  CupertinoPicker iOSPicker() {
    List<Text> pickerItems = [];
    for (String currency in currenciesList) {
      var newPicker = Text(
        currency,
      );
      pickerItems.add(newPicker);
    }

    return CupertinoPicker(
      backgroundColor: Colors.lightBlue,
      itemExtent: 32.0,
      onSelectedItemChanged: (selectedIntex) {
        print(selectedIntex);
      },
      children: pickerItems,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('🤑 Coin Ticker'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Column(
            children: <Widget>[
              ReuseableCard(
                cryptoName: 'BTC',
                currencyName: selectedCurrency,
                currencyValue:
                    currencyValue.length == 0 ? 'Loading' : currencyValue[0],
              ),
              ReuseableCard(
                cryptoName: 'LTH',
                currencyName: selectedCurrency,
                currencyValue:
                    currencyValue.length == 0 ? 'Loading' : currencyValue[1],
              ),
              ReuseableCard(
                cryptoName: 'ETC',
                currencyName: selectedCurrency,
                currencyValue:
                    currencyValue.length == 0 ? 'Loading' : currencyValue[2],
              ),
            ],
          ),
          Container(
            height: 150.0,
            alignment: Alignment.center,
            padding: EdgeInsets.only(bottom: 30.0),
            color: Colors.lightBlue,
            child: Platform.isIOS ? iOSPicker() : androidPicker(),
          ),
        ],
      ),
    );
  }
}

class ReuseableCard extends StatelessWidget {
  ReuseableCard({this.cryptoName, this.currencyName, this.currencyValue});
  final String cryptoName;
  final String currencyName;
  final String currencyValue;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(7.0, 18.0, 18.0, 0),
      child: Card(
        color: Colors.lightBlueAccent,
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 28.0),
          child: Text(
            '1 $cryptoName = $currencyValue $currencyName',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 20.0,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
