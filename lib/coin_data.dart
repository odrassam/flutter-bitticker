import 'dart:convert';
import 'package:http/http.dart' as http;
import 'network.dart';

String apiURL = 'https://apiv2.bitcoinaverage.com/indices/global/ticker/';

class CoinData {
  Future<dynamic> showValue( String crypto, String currency) async {
    NetworkHelper networkHelper = await NetworkHelper('$apiURL$crypto$currency');
    var getData = networkHelper.getData();
    return getData;
  }
}
const List<String> currenciesList = [
  'AUD',
  'BRL',
  'CAD',
  'CNY',
  'EUR',
  'GBP',
  'HKD',
  'IDR',
  'ILS',
  'INR',
  'JPY',
  'MXN',
  'NOK',
  'NZD',
  'PLN',
  'RON',
  'RUB',
  'SEK',
  'SGD',
  'USD',
  'ZAR'
];

const List<String> cryptoList = [
  'BTC',
  'ETH',
  'LTC',
];

